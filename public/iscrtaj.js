function iscrtajRaspored(div,dani,satPocetak,satKraj)
{
if(div == null){
    alert("Greska, paramtera null.");
}
if((satKraj - Number.parseInt(satKraj)) > 0 || (satPocetak - Number.parseInt(satPocetak)) > 0 || satPocetak >= satKraj || satPocetak < 0 || satKraj < 0 || satPocetak > 24 || satKraj > 24){
    div.innerHTML = "Greska";
    return;
}
var table = document.createElement('table');
var tableBody = document.createElement('tbody');
var red, kolona;
for(red = 1; red <= dani.length+1; red++){

    var redTabelaMoja = document.createElement('tr');

    if(red == 1 ){
        for(kolona = 0 ; kolona <= satKraj; kolona++){

            if(kolona >= satPocetak && kolona < satKraj)
            {
                var kolonaTabelaMoja = document.createElement('td');
                var spanTag = document.createElement('span');
                kolonaTabelaMoja.setAttribute("class","sati");
                if((kolona%2==0 && kolona<10))  spanTag.innerHTML = "0" + kolona + ":00";
                else if((kolona%2==1 && kolona>=15) || kolona == 12 || kolona == 10) spanTag.innerHTML =  kolona + ":00";
                kolonaTabelaMoja.setAttribute("colspan","2");   
                kolonaTabelaMoja.appendChild(document.createTextNode('\u0020'));
                kolonaTabelaMoja.appendChild(spanTag);
                redTabelaMoja.appendChild(kolonaTabelaMoja);
            }
        }
    }else
    {
        for(kolona = 1 ; kolona <= ((satKraj-satPocetak) * 2 ) + 1; kolona++)
        {
            var kolonaTabelaMoja = document.createElement('td');
            var span = document.createElement('span');
            if(kolona % 2 == 0){
                kolonaTabelaMoja.setAttribute("class","neparna-celija");
            }else if(kolona %2==1 && kolona != 1){
                kolonaTabelaMoja.setAttribute("class","parna-celija");
            }else if(kolona %2==1 && kolona == 1){
                kolonaTabelaMoja.setAttribute("class","kolona-dani")
                span.innerHTML = dani[red-2];
            }else
                kolonaTabelaMoja.setAttribute("class","prazna-celija");
            kolonaTabelaMoja.appendChild(document.createTextNode('\u0020'));
            kolonaTabelaMoja.appendChild(span);
            redTabelaMoja.appendChild(kolonaTabelaMoja);
        }
    }
    tableBody.appendChild(redTabelaMoja);
}
table.appendChild(tableBody);
div.appendChild(table);
}

function dodajAktivnost(raspored, naziv, tip, vrijemePocetak, vrijemeKraj,dan){

    if(raspored == null){
        alert("Greška - raspored nije kreiran");
    }
    var mojRaspored = document.getElementById(raspored.id).children;
    var pocetnoVrijemeTekst = mojRaspored[0].rows[0].cells[0].children[0].textContent;
    if(pocetnoVrijemeTekst == ""){
        pocetnoVrijemeTekst = mojRaspored[0].rows[0].cells[1].children[0].textContent;
        var vrijemePocetkaRasporeda = parseInt(pocetnoVrijemeTekst);
        vrijemePocetkaRasporeda -= 1;
    }
    else 
        var vrijemePocetkaRasporeda = parseInt(pocetnoVrijemeTekst);
    var krajRasporeda = Number.parseInt(mojRaspored[0].rows[0].cells.length) + vrijemePocetkaRasporeda;
    var daLiJeDanIspravan = false;
    var daLiJeVrijemeIspravno = false;
    var trajanjeAktivnosti = (vrijemeKraj - vrijemePocetak)*2;
    for (var r = 0; r < mojRaspored[0].rows.length; r++) {
        var red = mojRaspored[0].rows;
        if(red[r].cells[0].children[0].textContent == dan){
            daLiJeDanIspravan = true;
            var pomocna = 0;
            for (var c = 0; c < red[r].cells.length; c++) {
                var kolona = red[r].cells;
                if(kolona[c].style.backgroundColor == 'lavender')
                {
                    pomocna += Number.parseInt(kolona[c].getAttribute("colspan")) -1;
                }
                if((vrijemePocetak*2)-(vrijemePocetkaRasporeda*2)+1  == c + pomocna)
                {
                    for(var k = c; k < c+trajanjeAktivnosti; k++){
                    
                        if(kolona[k].style.backgroundColor == 'lavender'){
                            daLiJeVrijemeIspravno = true;
                            break;
                        }
                    }
                    if(daLiJeVrijemeIspravno){
                        alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                        break;
                    } else {
                        kolona[c].setAttribute("colspan",trajanjeAktivnosti);
                        kolona[c].innerHTML = naziv + "<br>" + tip;
                        kolona[c].style.backgroundColor = 'lavender';
                        kolona[c].style.textAlign = 'center';
                        for(var k = 1 ; k < trajanjeAktivnosti; k++){
                            red[r].deleteCell(c+1);
                        }
                        if(vrijemeKraj - Number.parseInt(vrijemeKraj) == 0 && vrijemePocetak - Number.parseInt(vrijemePocetak) == 0){
                            kolona[c].setAttribute("class","slucaj1");
                        }else if(vrijemePocetak - Number.parseInt(vrijemePocetak) == 0 && vrijemeKraj - Number.parseInt(vrijemeKraj) > 0){
                            kolona[c].setAttribute("class","slucaj2");
                        }else if(!Number.isInteger(vrijemePocetak) && vrijemeKraj - Number.parseInt(vrijemeKraj) == 0){
                            kolona[c].setAttribute("class","slucaj3");
                        }else {
                            kolona[c].setAttribute("class","slucaj4");
                        }
                        daLiJeVrijemeIspravno= true;
                        break;
                    }             
                }
            }
            if(!daLiJeVrijemeIspravno){
                alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                break;
            }else   break;
        }
    }
    if(!daLiJeDanIspravan){
        alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    }
}

