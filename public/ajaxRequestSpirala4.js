function  getGrupeAjax()  
 //učitava grupe u dropdown
{
    var grupe;
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200)   
        {
            grupe = JSON.parse(this.responseText);
            var grupeDD = document.getElementById('grupa');  
            var i = 1;
            for (var g in grupe) {
                grupeDD.options[i++] = new Option(grupe[g].naziv);   
            }
            
        }
    };

    ajax.open("GET","http://localhost:3000/v2/Grupa",true);  
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send();
}

function dodajStudentaAjax(tekst,grupa)
{
    var studentiJSON = csvJSON(tekst);
    document.getElementById('unosStudenta').value = studentiJSON;
    
    
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) 
            {
               // povratna += this.response;
                document.getElementById('unosStudenta').value = this.response.replaceAll("\"", "");   //this.response je povratna sa servera, bit će "" ako je uspješno dodan, a onaj dugi tekst ako nije ok
                //document.getElementById('unosStudenta').value += "\n"; 
            }   
        };
        ajax.open("POST","http://localhost:3000/v2/studentGrupa",true);  //slati na grupu
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify({studenti: studentiJSON, grupa:grupa}));

}



function csvJSON(csv){

    var lines=csv.split("\n");
  
    var result = [];
  
    // NOTE: If your columns contain commas in their values, you'll need
    // to deal with those before doing the next step 
    // (you might convert them to &&& or something, then covert them back later)
    // jsfiddle showing the issue https://jsfiddle.net/
    var headers=["ime","index"];
  
    for(var i=0;i<lines.length;i++){
  
        var obj = {};
        var currentline=lines[i].split(",");
  
        for(var j=0;j<headers.length;j++){
            obj[headers[j]] = currentline[j];
        }
  
        result.push(obj);
  
    }
  
    //return result; //JavaScript object
    return JSON.stringify(result); //JSON
  }





function parseCSV(tekst)
{
    var linije = tekst.split("\n");
    var studentiLista = [];
    for(var i in linije)
    {
        var ime = linije[i].split(",")[0];
        var index = linije[i]. split(",")[1];
        studentiLista.push([ime,index]);
    }
    return studentiLista;

}