let assert = chai.assert;
describe('rasporedModul', function() {
 describe('iscrtajRaspored()', function() {
   it('provjera naziva dana', function() {
        let okvir=document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8,21);
        let tabele = document.getElementsByTagName("table");
        let tabela = tabele[tabele.length-1]
        let redovi = tabela.getElementsByTagName("tr");
        let kolone = redovi[1].getElementsByTagName("td");
        assert.equal(kolone[0].children[0].innerText,"Ponedjeljak","Dan mora biti Ponedjeljak");
        okvir.innerHTML = "";
   });

    it('validnost vremena v1', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],0,26);
        assert.equal(okvir.textContent ,"Greska");
        okvir.innerHTML = "";
   });

   it('validnost vremena v2', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],-5,19);
        assert.equal(okvir.textContent ,"Greska");
        okvir.innerHTML = "";
   });

   it('validnost vremena v3', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,8);
        assert.equal(okvir.textContent ,"Greska");
        okvir.innerHTML = "";
   });

   it('validnost vremena v4', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8.5,18.5);
        assert.equal(okvir.textContent ,"Greska");
        okvir.innerHTML = "";
   });

   it('raspored koji traje jedan dan i jedan sat', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8,9);
        let tabele = document.getElementsByTagName("table");
        let tabela = tabele[tabele.length-1]
        let redovi = tabela.getElementsByTagName("tr");
        let kolone = redovi[1].getElementsByTagName("td");
        assert.equal(redovi.length,2,"Broj redova mora biti 2");
        assert.equal(kolone.length,3,"Broj kolona mora biti 3");
        okvir.innerHTML = "";
   });

   it('standardno iscrtavanje rasporeda', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,19);
        let tabele = document.getElementsByTagName("table");
        let tabela = tabele[tabele.length-1]
        let redovi = tabela.getElementsByTagName("tr");
        assert.equal(redovi.length,6,"Broj redova mora biti 6");
        okvir.innerHTML = "";
   });
   
   it('div parametar je null, ispitivanje greska', function() {
        let okvir = null
        assert.equal(rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,19) ,"Greska, paramtera null.");
   });

   it('kreiranje rasporeda za citavu sedmicu', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak", "Subota", "Nedjelja"],0,24);
        let tabele = document.getElementsByTagName("table");
        let tabela = tabele[tabele.length-1]
        let redovi = tabela.getElementsByTagName("tr");
        let kolone = redovi[1].getElementsByTagName("td");
        assert.equal(redovi.length,8,"Broj redova treba biti 8");
        assert.equal((kolone.length-1)/2,24,"Broj kolona treba biti 24");
        okvir.innerHTML = "";
   });

   it('raspored bez dana', function() {
    let okvir = null;
    assert.equal(rasporedModul.iscrtajRaspored(okvir,[],8,21),"Uspjesno");
    okvir.innerHTML = "";
    });

 });

 describe('dodajAktivnost()', function() {

    it('standardno dodavanje aktivnosti', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 20);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",9,12,"Ponedjeljak");
        okvir.innerHTML = "";
    });

    it('primjer sa spirale 2', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",9,12,"Ponedjeljak");
        rasporedModul.dodajAktivnost(okvir,"WT","vježbe",12,13.5,"Ponedjeljak");
        rasporedModul.dodajAktivnost(okvir,"RMA","predavanje",14,17,"Ponedjeljak");
        rasporedModul.dodajAktivnost(okvir,"RMA","vježbe",12.5,14,"Utorak");
        rasporedModul.dodajAktivnost(okvir,"DM","tutorijal",14,16,"Utorak");
        rasporedModul.dodajAktivnost(okvir,"DM","predavanje",16,19,"Utorak");
        rasporedModul.dodajAktivnost(okvir,"OI","predavanje",12,15,"Srijeda");
        okvir.innerHTML = "";

    });

    it('dodavanje aktivnosti tako da raspored bude popunjen', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8, 20);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8,20,"Ponedjeljak");
        rasporedModul.dodajAktivnost(okvir,"WT","vjezbe",8,20,"Utorak");
        rasporedModul.dodajAktivnost(okvir,"RG","predavanje",8,20,"Srijeda");
        rasporedModul.dodajAktivnost(okvir,"RG","vjezbe",8,20,"Četvrtak");
        rasporedModul.dodajAktivnost(okvir,"OIS","predavanje",8,20,"Petak");
        okvir.innerHTML = "";
    });

    it('dodavanje jedne aktivnosti koja traje cijeli dan', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 12);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8,12,"Ponedjeljak");
        let tabele = document.getElementsByTagName("table");
        let tabela = tabele[tabele.length-1]
        let redovi = tabela.getElementsByTagName("tr");
        let kolone = redovi[1].getElementsByTagName("td");
        assert.equal(kolone.length,2,"Broj kolona treba biti 2");
        okvir.innerHTML = "";
    });

    it('dodavanje krajnjih slucajeva', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 20);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8,8.5,"Ponedjeljak");
        rasporedModul.dodajAktivnost(okvir,"WT","vjezbe",19.5,20,"Ponedjeljak");
        okvir.innerHTML = "";
    });

    it('dodavanje jedne aktivnosti koja pocinje i zavrsava na vrijednost koja nije cijela', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 20);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8.5,19.5,"Ponedjeljak");
        okvir.innerHTML = "";
    });

    it('validnost vremena dodavanja aktivnosti v1', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 20);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8,11,"Ponedjeljak");
        assert.equal(rasporedModul.dodajAktivnost(okvir,"WT","vjezbe",9,14,"Ponedjeljak"),"Uspjesno");
        okvir.innerHTML = "";

    });

    it('validnost vremena dodavanja aktivnosti v2', function() {
        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 20);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8,15,"Ponedjeljak");
        assert.equal(rasporedModul.dodajAktivnost(okvir,"WT","vjezbe",9,14,"Ponedjeljak"),"Uspjesno");
        okvir.innerHTML = "";
    });

    it('validnost vremena dodavanja aktivnosti v3', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 20);
        rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8,15,"Ponedjeljak");
        assert.equal(rasporedModul.dodajAktivnost(okvir,"WT","vjezbe",9,11,"Ponedjeljak"),"Uspjesno");
        okvir.innerHTML = "";
    });

    it('validnost dana pri dodavanju aktivnosti v1', function() {

        let okvir = document.getElementById("ispis");
        okvir.innerHTML = "";
        rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak"],8, 20);
        assert.equal(rasporedModul.dodajAktivnost(okvir,"WT","predavanje",8,20,"Utorak"),"Upjesno");
        okvir.innerHTML = "";
    });

 });
});
