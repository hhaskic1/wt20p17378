var rasporedModul = (function (){
    
    var iscrtajRaspored = function(div,dani,satPocetak,satKraj)
    {
    if(div == null){
        alert("Greska, paramtera null.");
        return("Greska, paramtera null.");
    }
    if((satKraj - Number.parseInt(satKraj)) > 0 || (satPocetak - Number.parseInt(satPocetak)) > 0 || satPocetak >= satKraj || satPocetak < 0 || satKraj < 0 || satPocetak > 24 || satKraj > 24){
        div.innerHTML = "Greska";
        return "Greska";
    }
    var table = document.createElement('table');
    var tableBody = document.createElement('tbody');
    var red, kolona;
    for(red = 1; red <= dani.length+1; red++){
    
        var redTabelaMoja = document.createElement('tr');
    
        if(red == 1 ){
            for(kolona = 0 ; kolona <= satKraj; kolona++){
    
                if(kolona >= satPocetak && kolona < satKraj)
                {
                    var kolonaTabelaMoja = document.createElement('td');
                    var spanTag = document.createElement('span');
                    kolonaTabelaMoja.setAttribute("class","sati");
                    if((kolona%2==0 && kolona<10))  spanTag.innerHTML = "0" + kolona + ":00";
                    else if((kolona%2==1 && kolona>=15) || kolona == 12 || kolona == 10) spanTag.innerHTML =  kolona + ":00";
                    kolonaTabelaMoja.setAttribute("colspan","2");   
                    kolonaTabelaMoja.appendChild(document.createTextNode('\u0020'));
                    kolonaTabelaMoja.appendChild(spanTag);
                    redTabelaMoja.appendChild(kolonaTabelaMoja);
                }
            }
        }else
        {
            for(kolona = 1 ; kolona <= ((satKraj-satPocetak) * 2 ) + 1; kolona++)
            {
                var kolonaTabelaMoja = document.createElement('td');
                var span = document.createElement('span');
                if(kolona % 2 == 0){
                    kolonaTabelaMoja.setAttribute("class","neparna-celija");
                }else if(kolona %2==1 && kolona != 1){
                    kolonaTabelaMoja.setAttribute("class","parna-celija");
                }else if(kolona %2==1 && kolona == 1){
                    kolonaTabelaMoja.setAttribute("class","kolona-dani")
                    span.innerHTML = dani[red-2];
                }else
                    kolonaTabelaMoja.setAttribute("class","prazna-celija");
                kolonaTabelaMoja.appendChild(document.createTextNode('\u0020'));
                kolonaTabelaMoja.appendChild(span);
                redTabelaMoja.appendChild(kolonaTabelaMoja);
            }
        }
        tableBody.appendChild(redTabelaMoja);
    }
    table.appendChild(tableBody);
    div.appendChild(table);
    return "Uspjesno";
    }

    var dodajAktivnost = function(raspored, naziv, tip, vrijemePocetak, vrijemeKraj,dan){

        if(raspored == null){
            alert("Greška - raspored nije kreiran");
            return "Greška - raspored nije kreiran";
        }
        var mojRaspored = document.getElementById(raspored.id).children;
        var pocetnoVrijemeTekst = mojRaspored[0].rows[0].cells[0].children[0].textContent;
        if(pocetnoVrijemeTekst == ""){
            pocetnoVrijemeTekst = mojRaspored[0].rows[0].cells[1].children[0].textContent;
            var vrijemePocetkaRasporeda = parseInt(pocetnoVrijemeTekst);
            vrijemePocetkaRasporeda -= 1;
        }
        else 
            var vrijemePocetkaRasporeda = parseInt(pocetnoVrijemeTekst);
        var krajRasporeda = Number.parseInt(mojRaspored[0].rows[0].cells.length) + vrijemePocetkaRasporeda;
        var daLiJeDanIspravan = false;
        var daLiJeVrijemeIspravno = false;
        var trajenjeAktivnosti = (vrijemeKraj - vrijemePocetak)*2;
        for (var r = 0; r < mojRaspored[0].rows.length; r++) {
            var red = mojRaspored[0].rows;
            if(red[r].cells[0].children[0].textContent == dan){
                daLiJeDanIspravan = true;
                var pomocna = 0;
                for (var c = 0; c < red[r].cells.length; c++) {
                    var kolona = red[r].cells;
                    if(kolona[c].style.backgroundColor == 'lavender')
                    {
                        pomocna += Number.parseInt(kolona[c].getAttribute("colspan")) -1;
                    }
                    if((vrijemePocetak*2)-(vrijemePocetkaRasporeda*2)+1  == c + pomocna)
                    {
                        for(var k = c; k < c+trajenjeAktivnosti; k++){
                        
                            if(kolona[k].style.backgroundColor == 'lavender'){
                                daLiJeVrijemeIspravno = true;
                                break;
                            }
                        }
                        if(daLiJeVrijemeIspravno){
                            alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                            return "Greška - već postoji termin u rasporedu u zadanom vremenu";
                        } else {
                            kolona[c].setAttribute("colspan",trajenjeAktivnosti);
                            kolona[c].innerHTML = naziv + "<br>" + tip;
                            kolona[c].style.backgroundColor = 'lavender';
                            kolona[c].style.textAlign = 'center';
                            for(var k = 1 ; k < trajenjeAktivnosti; k++){
                                red[r].deleteCell(c+1);
                            }
                            if(vrijemeKraj - Number.parseInt(vrijemeKraj) == 0 && vrijemePocetak - Number.parseInt(vrijemePocetak) == 0){
                                kolona[c].setAttribute("class","punapuna");
                            }else if(vrijemePocetak - Number.parseInt(vrijemePocetak) == 0 && vrijemeKraj - Number.parseInt(vrijemeKraj) > 0){
                                kolona[c].setAttribute("class","punaiscrtana");
                            }else if(!Number.isInteger(vrijemePocetak) && vrijemeKraj - Number.parseInt(vrijemeKraj) == 0){
                                kolona[c].setAttribute("class","iscrtanapuna");
                            }else {
                                kolona[c].setAttribute("class","iscrtanaiscrtana");
                            }
                            daLiJeVrijemeIspravno= true;
                            break;
                        }             
                    }
                }
                if(!daLiJeVrijemeIspravno){
                    alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                    return "Greška - već postoji termin u rasporedu u zadanom vremenu";
                }else   break;
            }
        }
        if(!daLiJeDanIspravan){
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
            return "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
        }
        return "Uspjesno";
    }

    return {
        iscrtajRaspored : iscrtajRaspored,
        dodajAktivnost : dodajAktivnost
    }
}());

//treba otkomentarsati ako se ne koriste testvi i radit ce

/*
let okvir=document.getElementById("tabela");
rasporedModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
rasporedModul.dodajAktivnost(okvir,"RG","predavanje",8,15,"Ponedjeljak");
rasporedModul.dodajAktivnost(okvir,"RG","vježbe",15,21,"Ponedjeljak");
rasporedModul.dodajAktivnost(okvir,"RG","tutorijal",8,8.5,"Utorak");
rasporedModul.dodajAktivnost(okvir,"VVS","predavanje",20.5,21,"Utorak");
rasporedModul.dodajAktivnost(okvir,"VVS","vjezbe",8.5,12,"Srijeda");
rasporedModul.dodajAktivnost(okvir,"WT","predavanja",12.5,17.5,"Srijeda");
rasporedModul.dodajAktivnost(okvir,"WT","vjezbe",18,20.5,"Srijeda");
rasporedModul.dodajAktivnost(okvir,"OOI","predavanja",8,21,"Četvrtak");
rasporedModul.dodajAktivnost(okvir,"OIS","predavanja",12,15,"Petak");
rasporedModul.dodajAktivnost(okvir,"OIS","vježbe",13,17,"Petak");

let okvir2 = document.getElementById("tabela2");
rasporedModul.iscrtajRaspored(okvir2,["Ponedjeljak","Utorak","Srijeda"],10,15);
rasporedModul.dodajAktivnost(okvir2,"RG","predavanje",10,11,"Ponedjeljak");
rasporedModul.dodajAktivnost(okvir2,"RG","vježbe",12,13,"Ponedjeljak");
rasporedModul.dodajAktivnost(okvir2,"RG","tutorijal",14,15,"Ponedjeljak");
rasporedModul.dodajAktivnost(okvir2,"VVS","predavanje",10,10.5,"Utorak");
rasporedModul.dodajAktivnost(okvir2,"VVS","vjezbe",11.5,12.5,"Utorak");
rasporedModul.dodajAktivnost(okvir2,"WT","predavanja",13,13.5,"Utorak");
rasporedModul.dodajAktivnost(okvir2,"WT","vjezbe",14.5,15,"Utorak");
rasporedModul.dodajAktivnost(okvir2,"OOI","predavanja",10.5,11.5,"Srijeda");
rasporedModul.dodajAktivnost(okvir2,"OIS","predavanja",12.5,13.5,"Srijeda");
rasporedModul.dodajAktivnost(okvir2,"OIS","vjezbe",13,15,"Srijeda");

*/
