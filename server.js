const fs = require('fs');
const csv = require('csv-parser');
const express = require('express');
const bodyParser = require('body-parser');
const { Console } = require('console');
const app = express();
app.use(express.static('public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


    const Sequelize = require('sequelize');
    const db = require("./db.js");


    app.post('/v2/Predmet',function(req,res){
        db.Predmet.create({naziv: req.body.naziv}).then(function(r){   
            res.writeHead(200, {'Content-Type': 'application/json'});  
            res.end();
        })
    });


    app.get('/v2/Predmet',function(req,res){
        db.Predmet.findAll().then(function(r){
                //var predmeti = [];
                //for(var i = 0; i < r.length; i++)    predmeti.push(r[i].naziv);  
                res.write(JSON.stringify(r))
            res.end();        
        }); 
    });

    app.delete('/v2/Predmet/:id',function(req,res){  
        db.Predmet.findAll({where:{id:req.params.id}}).then(function(r){
                r.forEach(element => {
                    element.destroy();
                });
            res.end();        
        });
    });

    app.put('/v2/Predmet/:id',function(req,res){  
        db.Predmet.update(
                        {naziv: req.body.naziv},
                        {where: {id:req.params.id}}
                        );
                        res.end();          
     });
        
   


    //AKTIVNOST
    app.post('/v2/Aktivnost',function(req,res){
        db.Aktivnost.create({naziv: req.body.naziv, pocetak:req.body.pocetak, kraj:req.body.kraj}).then(function(r){   
            res.writeHead(200, {'Content-Type': 'application/json'});  
            res.end();
        })
    });


    app.get('/v2/Aktivnost',function(req,res){
        db.Aktivnost.findAll().then(function(r){
                //var predmeti = [];
                //for(var i = 0; i < r.length; i++)    predmeti.push(r[i].naziv);  
                res.write(JSON.stringify(r));
            res.end();        
        }); 
    });

    app.delete('/v2/Aktivnost/:id',function(req,res){  
        db.Aktivnost.findAll({where:{id:req.params.id}}).then(function(r){
                r.forEach(element => {
                    element.destroy();
                });
            res.end();        
        });
    });

    app.put('/v2/Aktivnost/:id',function(req,res){  
        db.Aktivnost.update(
                        {naziv: req.body.naziv, pocetak:req.body.pocetak, kraj:req.body.kraj},
                        {where: {id:req.params.id}}
                        );
                        res.end();          
     });


     //STUDENT
    app.post('/v2/Student',function(req,res){
        db.Student.findAll({where:{index:req.body.index}}).then(function(r){   //pretražujem studente sa ovim indexom
            var result;
            if(r.length > 0)   //r je povratna pretrage, ako ima nekih rezultata znači da je našao već u bazi studenta s tim indexom, i onda nećemo da ga dodajemo već vraća ovu poruku
            {
                result = "Student "+ req.body.ime +" nije kreiran jer postoji student "+ r[0].ime +" sa istim indexom " + req.body.index;  
                res.writeHead(200, {'Content-Type': 'application/json'});  
                res.write(JSON.stringify(result));
                res.end();  
            }
            else db.Student.create({ime: req.body.ime, index:req.body.index}).then(function(r){    //a ako nemamo u bazi već studenta, onda ga dodamo i vratim prazan string
               res.writeHead(200, {'Content-Type': 'application/json'});  
                res.write(JSON.stringify(""));
                res.end();  
                //res.end(); 
            }) 
            
    });
        
    });


    app.get('/v2/Student',function(req,res){
        db.Student.findAll().then(function(r){
                //var predmeti = [];
                //for(var i = 0; i < r.length; i++)    predmeti.push([r[i].ime,r[i].index]);   
                res.write(JSON.stringify(r))
            res.end();        
        }); 
    });

    app.delete('/v2/Student/:id',function(req,res){  
        db.Student.findAll({where:{id:req.params.id}}).then(function(r){
                r.forEach(element => {
                    element.destroy();
                });
            res.end();        
        });
    });

    app.put('/v2/Student/:id',function(req,res){  
        db.Student.update(
                        {ime: req.body.ime, index:req.body.index},
                        {where: {id:req.params.id}}
                        );
                        res.end();          
     });

          //DAN: parametar naziv

    //post ruta za Dan -> dan ima parametar naziv
    app.post('/v2/Dan',function(req,res){
        db.Dan.create({naziv: req.body.naziv}).then(function(r){   
            res.writeHead(200, {'Content-Type': 'application/json'});  //dodati povratnu
            res.end();
        })
    });


    //get ruta za Dan -> vraća sve dane 
    app.get('/v2/Dan',function(req,res){
        db.Dan.findAll().then(function(r){
                //var dani = [];
                //for(var i = 0; i < r.length; i++)    dani.push(r[i].naziv);  
                res.write(JSON.stringify(r))
            res.end();        
        }); 
    });

    //delete ruta za DAn, briše Dan sa zadanim id brojem
    app.delete('/v2/Dan/:id',function(req,res){  
        db.Dan.findAll({where:{id:req.params.id}}).then(function(r){
                r.forEach(element => {
                    element.destroy();
                });
            res.end();        
        });
    });


    //update ruta za Dane, pošalje joj se id kao parametar i nove vrijednosti u body
    //i ažurira onda vrijednost u tabeli za taj id
    app.put('/v2/Dan/:id',function(req,res){  
        db.Dan.update(
                        {naziv: req.body.naziv},
                        {where: {id:req.params.id}}
                        );
                        res.end();          
     });



        

//GRUPA : parametar naziv

//post ruta za Grupa -> predmet ima parametar naziv 
app.post('/v2/Grupa',function(req,res){
    db.Grupa.create({naziv: req.body.naziv}).then(function(r){   
        res.writeHead(200, {'Content-Type': 'application/json'});  
        res.end();
    })
});


//get ruta za Grupa -> vraća sve grupe 
app.get('/v2/Grupa',function(req,res){
    db.Grupa.findAll().then(function(r){
            //var grupe = [];
            //for(var i = 0; i < r.length; i++)    grupe.push(r[i].naziv);  
            res.write(JSON.stringify(r))
        res.end();        
    }); 
});

//delete ruta za Grupu, briše grupu sa zadanim id brojem
app.delete('/v2/Grupa/:id',function(req,res){  
    db.Grupa.findAll({where:{id:req.params.id}}).then(function(r){
            r.forEach(element => {
                element.destroy();
            });
        res.end();        
    });
});


//update ruta za Grupu, pošalje joj se id kao parametar i nove vrijednosti u body
//i ažurira onda vrijednost u tabeli za taj id
app.put('/v2/Grupa/:id',function(req,res){  
    db.Grupa.update(
                    {naziv: req.body.naziv},
                    {where: {id:req.params.id}}
                    );
                    res.end();          
 });


//TIP

//post ruta za TIP 
app.post('/v2/Tip',function(req,res){
    db.Tip.create({naziv: req.body.naziv}).then(function(r){   
        res.writeHead(200, {'Content-Type': 'application/json'});  
        res.end();
    })
});


//get ruta za Grupa -> vraća tip 
app.get('/v2/Tip',function(req,res){
    db.Tip.findAll().then(function(r){
            //var tipovi = [];
            //for(var i = 0; i < r.length; i++)    tipovi.push(r[i].naziv);  
            res.write(JSON.stringify(r))
        res.end();        
    }); 
});

//delete ruta za Tip, briše tip sa zadanim id brojem
app.delete('/v2/Tip/:id',function(req,res){  
    db.Tip.findAll({where:{id:req.params.id}}).then(function(r){
            r.forEach(element => {
                element.destroy();
            });
        res.end();        
    });
});


//update ruta za Predmet, pošalje joj se id kao parametar i nove vrijednosti u body
//i ažurira onda vrijednost u tabeli za taj id
app.put('/v2/Tip/:id',function(req,res){  
    db.Tip.update(
                    {naziv: req.body.naziv},
                    {where: {id:req.params.id}}
                    );
                    res.end();          
 });

 
 
 ///grupa-student 
 app.post('/v2/studentGrupa',function(req,res){
    var povratna = "";
    //dobijem niz studenata i grupu
     var studenti = JSON.stringify(req.body.studenti).replace("[","").replace("]","").replace(/\\/g,"");
     var listaStudenata = studenti.split("},{");

     var grupa = req.body.grupa;
    var zaPromise = [];
     for(var i in listaStudenata)
     {  console.log("{" + listaStudenata[i].replace(/"{/,"").replace(/}"/,"") + "}");
         var jedanRed = JSON.parse("{" + listaStudenata[i].replace(/"{/,"").replace(/}"/,"") + "}");
        //console.log(jedanRed);
        zaPromise.push(db.Student.findAll({where:{index:jedanRed.index}}).then(function(r){   //pretražujem studente sa ovim indexom
            var result;
            if(r.length > 0)   //r je povratna pretrage, ako ima nekih rezultata znači da je našao već u bazi studenta s tim indexom, i onda nećemo da ga dodajemo već vraća ovu poruku
            {
                povratna += "Student "+ jedanRed.ime +" nije kreiran jer postoji student "+ r[0].ime +" sa istim indexom " + jedanRed.index + "\n";
               // res.writeHead(200, {'Content-Type': 'application/json'});  
                //res.write(JSON.stringify(result));
                //res.end();  
                     
                zaPromise.push( db.Grupa.findAll({where:{naziv:grupa}}).then(function(g){ 
                        grupaID = g[0].id;  
                        zaPromise.push(db.GrupaStudent.findAll({where:{studentID:r[0].id}}).then(function(k){
                            if(k.length > 0)
                            {
                                zaPromise.push(db.GrupaStudent.update(
                                
                                    {studentID: r[0].id, grupaID:grupaID},
                                    {where:{id:k[0].id}}
                                ));
                            }
                            else  zaPromise.push(db.GrupaStudent.create({studentID: r[0].id, grupaID:grupaID}).then(function(r){ 
                                //console.log(req.body.studentID);  
                                //res.end();
                            }));
                        }));
                       
            
                    }));
               //res.end();
            }
            else zaPromise.push(db.Student.create({ime: jedanRed.ime, index:jedanRed.index}).then(function(r){    //a ako nemamo u bazi već studenta, onda ga dodamo i vratim prazan string
               zaPromise.push(db.Student.findAll({where:{index:jedanRed.index}}).then(function(r){ 
                    studentID = r[0].id;       
                    zaPromise.push(db.Grupa.findAll({where:{naziv:grupa}}).then(function(r){ 
                        grupaID = r[0].id;  
                        
                        zaPromise.push(db.GrupaStudent.create({studentID: studentID, grupaID:grupaID}).then(function(r){ 
                           
                        }))
            
                    }));
               
               
               
               
               
                }));
                //res.end(); 
            }) )
            
         })
        )
     }
     Promise.all(zaPromise).then(function(k)
     {
        res.writeHead(200, {'Content-Type': 'application/json'});  
        res.write(JSON.stringify(povratna));
        res.end();
        console.log("povratna:" + povratna);
     });
    
    
    
    
});
module.exports = app.listen(3000);