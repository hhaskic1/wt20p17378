
/*const Sequelize = require('sequelize');
const sequelize = new Sequelize('wt2017378', 'root', 'root', {
   host: '127.0.0.1',
   dialect: 'mysql',
   port: 3000
   
});

const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

db.Predmet = require(__dirname+'/Predmet.js')(sequelize, Sequelize.DataTypes);
db.Dan = require(__dirname+'/Dan.js')(sequelize, Sequelize.DataTypes);
db.Aktivnost = require(__dirname+'/Aktivnost.js')(sequelize, Sequelize.DataTypes);
db.Student = require(__dirname+'/Student.js')(sequelize, Sequelize.DataTypes);
db.Tip = require(__dirname+'/Tip.js')(sequelize, Sequelize.DataTypes);
db.Grupa = require(__dirname+'/Grupa.js')(sequelize, Sequelize.DataTypes);
db.Predmet.sync();

//relacije:

Predmet 1-N Grupa
Aktivnost N-1 Predmet
Aktivnost N-0 Grupa ?
Aktivnost N-1 Dan
Aktivnost N-1 Tip
Student N-M Grupa


db.Predmet.hasMany(db.Grupa,{as:'grupePredmet'});
db.Grupa.belongsTo(db.Predmet);

db.Predmet.hasMany(db.Aktivnost,{as:'aktivnosti'});
db.Aktivnost.belongsTo(db.Predmet);

db.Grupa.hasMany(db.Aktivnost); 
db.Aktivnost.belongsTo(db.Grupa);

db.Dan.hasMany(db.Aktivnost,{as:'aktivnostDan'});
db.Aktivnost.belongsTo(db.Dan);

db.Tip.hasMany(db.Aktivnost,{as:'aktivnostTip'});
db.Aktivnost.belongsTo(db.Tip);

db.Student.belongsToMany(db.Grupa,{
    through:'student_grupa',
    as:'Grupa',
    foreignKey:'studentID'
});

db.Grupa.belongsToMany(db.Student,{
    through:'student_grupa',
    as:'Student',
    foreignKey:'grupaID'
});

module.exports=db;


*/