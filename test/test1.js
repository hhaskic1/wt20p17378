let server = require('../haris');
let chai = require('chai');
const fs = require('fs'), readline = require('readline');
const csv = require('csv-parser');
let chaiHttp = require('chai-http');
var assert = require('assert');
const { json } = require('express');
const { debug } = require('console');

chai.should();
chai.use(chaiHttp);


let testData = class {
    constructor(metoda, url, body, output) {
      this.metoda = metoda;
      this.url = url;
      this.body = body;
      this.output = output;
    }
  };



var readFile = function(filename){
    return fs.readFileSync(filename).toString();
}


var data = readFile("testniPodaci.txt");
var testniPodaci = [];
var lines  = data.split('\n');



function runTest(obj) {
    var parsirano = obj.split(",");
    if(parsirano[0] == "DELETE" && parsirano[1] == "/all")
    {
        describe("test delete",  () =>{
            describe("DELETE /all" , ()=>
            {
                it('brisanje',  ()=>
                {
                    chai.request(server)
                    .delete(parsirano[1])
                    .end((err, res) =>
                    {
                        assert.strictEqual(JSON.stringify(res.body), parsirano[3].replace(/\\/g,"").replace("\r",""));
                    });
                });
            });

        });

    }
    
    if(parsirano[0] == "DELETE" && parsirano[1].split("/")[1] == "aktivnost")
    {
        
        describe("test delete",  () =>{
            describe("DELETE /aktivnost/:naziv" , ()=>
            {
                it('brisanje',  ()=>
                {
                    chai.request(server)
                    .delete(parsirano[1])
                    .end((err, res) =>
                    {
                        assert.strictEqual(JSON.stringify(res.body), parsirano[3].replace(/\\/g,"").replace("\r",""));
                    });
                });
            });

        });

    }

    if(parsirano[0] == "DELETE" && parsirano[1].split("/")[1] == "predmet")
    {
        
        describe("test delete",  () =>{
            describe("DELETE /predmet/:naziv" , ()=>
            {
                it('brisanje',  ()=>
                {
                    chai.request(server)
                    .delete(parsirano[1])
                    .end((err, res) =>
                    {
                        assert.strictEqual(JSON.stringify(res.body), parsirano[3].replace(/\\/g,"").replace("\r",""));
                    });
                });
            });

        });

    }



    if(parsirano[0] == "GET" && parsirano[1] == "/predmeti")
    {
        describe("test get",  () =>{
            describe("GET /predmeti" , ()=>
            {
                it('get',  ()=>
                {
                    chai.request(server)
                    .get(parsirano[1])
                    .end((err, res) =>
                    {
                        var expected ="[" + obj.split("[")[1];
                        assert.strictEqual(JSON.stringify(res.body), expected.replace(/\\/g,"").replace("\r",""));
                    });
                });
            });

        });

    }
    
    if(parsirano[0] == "GET" && parsirano[1].split("/")[1] == "predmet" && parsirano[1].split("/")[3] =="aktivnost")
    {
        describe("test get predmet/:naziv/aktivnost",  () =>{
            describe("GET /predmet/:naziv/aktivnost" , ()=>
            {
                it('get',  ()=>
                {
                    chai.request(server)
                    .get(parsirano[1])
                    .end((err, res) =>
                    {
                        var expected ="[" + obj.split("[")[1];
                        assert.strictEqual(JSON.stringify(res.body), expected.replace(/\\/g,"").replace("\r",""));
                    });
                });
            });

        });

    }

    if(parsirano[0] == "GET" && parsirano[1] == "/aktivnosti")
    {
        describe("test get",  () =>{
            describe("GET /aktivnosti" , ()=>
            {
                it('get',  ()=>
                {
                    chai.request(server)
                    .get(parsirano[1])
                    .end((err, res) =>
                    {
                        var expected ="[" + obj.split("[")[1];
                        assert.strictEqual(JSON.stringify(res.body), expected.replace(/\\/g,"").replace("\r",""));
                    });
                });
            });

        });

    }


    if(parsirano[0] == "POST" && parsirano[1] == "/predmet")
    {
        describe("test post",  () =>{
            describe("POST /predmet" , ()=>
            {
                it('post',  ()=>
                {
                    
                    chai.request(server)
                    .post(parsirano[1])
                    .set('content-type', 'application/json')
                    .send((parsirano[2].replace(/\\/g,"")))
                    .end((err, res) =>
                    {
                        
                        assert.strictEqual(JSON.stringify(res.body), parsirano[3].replace(/\\/g,"").replace("\r",""));
                    });
                });
            });
        });
    }


    if(parsirano[0] == "POST" && parsirano[1] == "/aktivnost")
    {
        describe("test post",  () =>{
            describe("POST /aktivnost" , ()=>
            {
                it('post',  ()=>
                {
                    var body = obj.split("{")[1].split("}")[0].trim();
                    
                    body += "}";
                    var body2 ="{";
                    body2 += body;
                    var bodyFinal = body2.trim();
                

                    var expected = obj.split("{")[2];
                    var exp = "{" + expected;
                    chai.request(server)
                    .post(parsirano[1])
                    .set('content-type', 'application/json')
                    .send(bodyFinal.replace(/\\/g,""))
                    .end((err, res) =>
                    {
                        assert.strictEqual(JSON.stringify(res.body), exp.replace(/\\/g,"").replace("\r",""));
                    });
                });
            });

        });

    }
    
}



describe("Testing routes", function () {
    for (let i = 0; i < lines.length ; i++) {
        var obj =lines[i];
        runTest(obj);
    }
});


