
const fs = require('fs');
const csv = require('csv-parser');
const express = require('express');
const bodyParser = require('body-parser');
const { Console } = require('console');
const app = express();
app.use(express.static('public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


const Sequelize = require('sequelize');
    const db = require("./db.js");


app.get('/v1/predmeti',function(req,res){
	let results = [];
            res.writeHead(200, {'Content-Type': 'application/json'});
            fs.createReadStream('predmeti.txt') 
                .pipe(csv())
                .on('data', (data) => results.push(data))
                .on('end', () => {
                    res.write(JSON.stringify(results))
                    res.end();
                });   
        });
		
 app.get('/v1/aktivnosti',function(req,res){   
	let results = [];
            res.writeHead(200, {'Content-Type': 'application/json'});
            fs.createReadStream('aktivnosti.txt') 
                .pipe(csv())
                .on('data', (data) => results.push(data))
                .on('end', () => {
                    res.write(JSON.stringify(results))
                    res.end();
                });   
    });
	
app.get('/v1/predmet/:naziv/aktivnost',function(req,res){   //vraća niz objekata aktivnosti za zadani predmet
	let results = [];  
            res.writeHead(200, {'Content-Type': 'application/json'});
            fs.createReadStream('aktivnosti.txt') 
                .pipe(csv())
                .on('data', (data) => {
					if(req.params.naziv == data['naziv']) results.push(data);
					})
                .on('end', () => {
                    res.write(JSON.stringify(results))
                    res.end();
                });   

    });
	

 app.post('/v1/predmet',function(req,res){
	   var imaGa = 0;
       let results2 = [];
       let tijeloZahtjeva = '';
	   let novaLinija;
	   let obj;
	   tijeloZahtjeva=req.body['naziv'];
	   novaLinija = "\n" + req.body['naziv'];
	   let poruka ="";
	   res.writeHead(200, {'Content-Type': 'application/json'});
        fs.createReadStream('predmeti.txt') 
                .pipe(csv())
                .on('data', (data) => results2.push(data))
                .on('end', () => {
                    const svi = JSON.parse(JSON.stringify(results2));                  
                    for(i in svi)
                    { 	
                        if(svi[i].naziv == tijeloZahtjeva)
                        {
                            imaGa = 1; 
                        }
                    }
					if(imaGa == 1) poruka = "Naziv predmeta postoji!";				
					else 
					{
						poruka = "Uspješno dodan predmet!";
						fs.appendFile('predmeti.txt',novaLinija,function(err)
						{ 
							if(err) throw err;
						});	
					}
					res.write(JSON.stringify({message:poruka}));
					res.end();
        });  
});
	
	
app.post('/v1/aktivnost',function(req,res)
    {
       tijeloZahtjeva=req.body;
	   let novaLinija ="\n" +  req.body['naziv'] +"," + req.body['tip'] + "," + req.body['pocetak'] + "," + req.body['kraj'] + "," + req.body['dan'] ;;
	   let obj;
	   var validacija = 1; //ulaz je ok 
	   let results2 = [];
	   
	   let poruka = "";
	   res.writeHead(200, {'Content-Type': 'application/json'});
	  //ako ima greška postaviti validacija = 0;

	  //VALIDACIJA POCETAK
	  //vrijemePocetak je broj od 0 do 24
	  if(parseFloat(req.body['pocetak']) < 0 || parseFloat(req.body['pocetak']) > 24) validacija = 0;
	  else if(req.body['naziv'] == "") validacija = 0;
	  //cijeli broj ili ima samo jedna decimala
	  else if(parseFloat(req.body['pocetak']) - parseFloat(req.body['pocetak']).toFixed(1) != 0) validacija = 0; //ok
	  //cijeli broj ili 0.5
	  else if(parseFloat(req.body['pocetak']).toFixed(1) - parseInt(req.body['pocetak']).toFixed(1) != 0 && parseFloat(req.body['pocetak']).toFixed(1) - parseInt(req.body['pocetak']).toFixed(1) != 0.5) validacija = 0;
		   
	  
	  // Parametar vrijemeKraj ima isti oblik kao vrijemePocetak 
	  else if(parseFloat(req.body['kraj']) < 0 || parseFloat(req.body['kraj']) > 24) validacija = 0;
	  //cijeli broj ili ima samo jedna decimala
	  else if(parseFloat(req.body['kraj']) - parseFloat(req.body['kraj']).toFixed(1) != 0) validacija = 0; //ok
	  //cijeli broj ili 0.5
	  else if(parseFloat(req.body['kraj']).toFixed(1) - parseInt(req.body['kraj']).toFixed(1) != 0 && parseFloat(req.body['kraj']).toFixed(1) - parseInt(req.body['kraj']).toFixed(1) != 0.5) validacija = 0;
		
	  //s tim da treba biti veći od vrijemePocetak
	  else if(parseFloat(req.body['kraj']) < parseFloat(req.body['pocetak'])) validacija = 0;
	  
	  
		  //ako već postoji termin koji se djelimično ili u potpunosti preklapa sa terminom kojeg pokušavamo dodati 
	 	 //učitamo sve aktivnosti 
		fs.createReadStream('aktivnosti.txt')
		.pipe(csv())
		.on('data', (data) => results2.push(data))
		.on('end', () => {
			const svi = JSON.parse(JSON.stringify(results2));                  
			for(i in svi) //sve aktivnosti, treba provjeriti svakoj kraj da je manji od početka naše i početak manji od kraja naše
			{ 	
				if(validacija == 1 &&  svi[i].dan == req.body['dan'])  //al samo ako je isti dan 
					if( 
						(parseFloat(svi[i].pocetak) < parseFloat(req.body['pocetak']) && parseFloat(svi[i].kraj) > parseFloat(req.body['pocetak']))
						||
						(parseFloat(svi[i].pocetak) < parseFloat(req.body['kraj']) && parseFloat(svi[i].kraj) > parseFloat(req.body['kraj']))
						||
						(parseFloat(svi[i].pocetak) == parseFloat(req.body['pocetak']) && parseFloat(svi[i].kraj) == parseFloat(req.body['kraj'])) 
						||
						(parseFloat(svi[i].pocetak) > parseFloat(req.body['pocetak']) && parseFloat(svi[i].kraj) < parseFloat(req.body['kraj'])) 
					) 
					{							
						validacija = 0; 
					}
			}

			if(validacija == 1)  //ako je sve ok upiši u fajl
			{
				poruka = "Uspješno dodana aktivnost!";
				fs.appendFile('aktivnosti.txt',novaLinija,function(err){ 
				if(err) throw err;
				
				});
			}
			else 
				poruka = "Aktivnost nije validna!";
			res.write(JSON.stringify({message:poruka}));
			res.end(); 	
		});  	
    });

const removeLines = (data, naziv) => {
	var lines  = data.split('\n');
	var newlines = "";
	var deleted = 0;
	for(i in lines)
	{
		if(lines[i].split(',')[0] != naziv && lines[i].split(',')[0] != '\n') 
		{
			newlines += (lines[i] + '\n');
			
		}
		else deleted++;
	}
    return [newlines,deleted];
}



app.delete('/v1/aktivnost/:naziv', function(req, res)
{	fs.readFile('aktivnosti.txt', 'utf8', (err, data) => { 
		if (err) res.json({message:"Greška - aktivnost nije obrisana!"});
		var ret = removeLines(data, req.params.naziv);
		fs.writeFile('aktivnosti.txt', ret[0], 'utf8', function(err) { 

			
			if (ret[1] == 0) res.json({message:"Greška - aktivnost nije obrisana!"});   
			else res.json({message:"Uspješno obrisana aktivnost!"});
			res.end();
		});
	});
});


app.delete('/v1/predmet/:naziv', function(req, res)
{	fs.readFile('predmeti.txt', 'utf8', (err, data) => { 
		if (err)  res.json({message:"Greška - predmet nije obrisan!"});
		var ret = removeLines(data, req.params.naziv);
		fs.writeFile('predmeti.txt', ret[0], 'utf8', function(err) { 
			if (ret[1] == 0) res.json({message:"Greška - predmet nije obrisan!"});  
			else res.json({message:"Uspješno obrisan predmet!"});
		});
	});
});


app.delete('/v1/all', function(req, res)
{
		fs.writeFile('predmeti.txt', 'naziv', 'utf8', function(err) { 
			fs.writeFile('aktivnosti.txt', 'naziv,tip,pocetak,kraj,dan', 'utf8', function(err) { 
				if (err) res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"});  
				res.json({message:"Uspješno obrisan sadržaj datoteka!"});
			});
		});
});
	


app.post('/v2/Predmet',function(req,res){
	db.Predmet.create({naziv: req.body.naziv}).then(function(r){   
		res.writeHead(200, {'Content-Type': 'application/json'});  
		res.end();
	})
});


app.get('/v2/Predmet',function(req,res){
	db.Predmet.findAll().then(function(r){
			//var predmeti = [];
			//for(var i = 0; i < r.length; i++)    predmeti.push(r[i].naziv);  
			res.write(JSON.stringify(r))
		res.end();        
	}); 
});

app.delete('/v2/Predmet/:id',function(req,res){  
	db.Predmet.findAll({where:{id:req.params.id}}).then(function(r){
			r.forEach(element => {
				element.destroy();
			});
		res.end();        
	});
});

app.put('/v2/Predmet/:id',function(req,res){  
	db.Predmet.update(
					{naziv: req.body.naziv},
					{where: {id:req.params.id}}
					);
					res.end();          
 });
	



//AKTIVNOST
app.post('/v2/Aktivnost',function(req,res){
	db.Aktivnost.create({naziv: req.body.naziv, pocetak:req.body.pocetak, kraj:req.body.kraj}).then(function(r){   
		res.writeHead(200, {'Content-Type': 'application/json'});  
		res.end();
	})
});


app.get('/v2/Aktivnost',function(req,res){
	db.Aktivnost.findAll().then(function(r){
			//var predmeti = [];
			//for(var i = 0; i < r.length; i++)    predmeti.push(r[i].naziv);  
			res.write(JSON.stringify(r));
		res.end();        
	}); 
});

app.delete('/v2/Aktivnost/:id',function(req,res){  
	db.Aktivnost.findAll({where:{id:req.params.id}}).then(function(r){
			r.forEach(element => {
				element.destroy();
			});
		res.end();        
	});
});

app.put('/v2/Aktivnost/:id',function(req,res){  
	db.Aktivnost.update(
					{naziv: req.body.naziv, pocetak:req.body.pocetak, kraj:req.body.kraj},
					{where: {id:req.params.id}}
					);
					res.end();          
 });


 //STUDENT
app.post('/v2/Student',function(req,res){
	db.Student.findAll({where:{index:req.body.index}}).then(function(r){   //pretražujem studente sa ovim indexom
		var result;
		if(r.length > 0)   //r je povratna pretrage, ako ima nekih rezultata znači da je našao već u bazi studenta s tim indexom, i onda nećemo da ga dodajemo već vraća ovu poruku
		{
			result = "Student "+ req.body.ime +" nije kreiran jer postoji student "+ r[0].ime +" sa istim indexom " + req.body.index;  
			res.writeHead(200, {'Content-Type': 'application/json'});  
			res.write(JSON.stringify(result));
			res.end();  
		}
		else db.Student.create({ime: req.body.ime, index:req.body.index}).then(function(r){    //a ako nemamo u bazi već studenta, onda ga dodamo i vratim prazan string
		   res.writeHead(200, {'Content-Type': 'application/json'});  
			res.write(JSON.stringify(""));
			res.end();  
			//res.end(); 
		}) 
		
});
	
});


app.get('/v2/Student',function(req,res){
	db.Student.findAll().then(function(r){
			//var predmeti = [];
			//for(var i = 0; i < r.length; i++)    predmeti.push([r[i].ime,r[i].index]);   
			res.write(JSON.stringify(r))
		res.end();        
	}); 
});

app.delete('/v2/Student/:id',function(req,res){  
	db.Student.findAll({where:{id:req.params.id}}).then(function(r){
			r.forEach(element => {
				element.destroy();
			});
		res.end();        
	});
});

app.put('/v2/Student/:id',function(req,res){  
	db.Student.update(
					{ime: req.body.ime, index:req.body.index},
					{where: {id:req.params.id}}
					);
					res.end();          
 });

	  //DAN: parametar naziv

//post ruta za Dan -> dan ima parametar naziv
app.post('/v2/Dan',function(req,res){
	db.Dan.create({naziv: req.body.naziv}).then(function(r){   
		res.writeHead(200, {'Content-Type': 'application/json'});  //dodati povratnu
		res.end();
	})
});


//get ruta za Dan -> vraća sve dane 
app.get('/v2/Dan',function(req,res){
	db.Dan.findAll().then(function(r){
			//var dani = [];
			//for(var i = 0; i < r.length; i++)    dani.push(r[i].naziv);  
			res.write(JSON.stringify(r))
		res.end();        
	}); 
});

//delete ruta za DAn, briše Dan sa zadanim id brojem
app.delete('/v2/Dan/:id',function(req,res){  
	db.Dan.findAll({where:{id:req.params.id}}).then(function(r){
			r.forEach(element => {
				element.destroy();
			});
		res.end();        
	});
});


//update ruta za Dane, pošalje joj se id kao parametar i nove vrijednosti u body
//i ažurira onda vrijednost u tabeli za taj id
app.put('/v2/Dan/:id',function(req,res){  
	db.Dan.update(
					{naziv: req.body.naziv},
					{where: {id:req.params.id}}
					);
					res.end();          
 });



	

//GRUPA : parametar naziv

//post ruta za Grupa -> predmet ima parametar naziv 
app.post('/v2/Grupa',function(req,res){
db.Grupa.create({naziv: req.body.naziv}).then(function(r){   
	res.writeHead(200, {'Content-Type': 'application/json'});  
	res.end();
})
});


//get ruta za Grupa -> vraća sve grupe 
app.get('/v2/Grupa',function(req,res){
db.Grupa.findAll().then(function(r){
		//var grupe = [];
		//for(var i = 0; i < r.length; i++)    grupe.push(r[i].naziv);  
		res.write(JSON.stringify(r))
	res.end();        
}); 
});

//delete ruta za Grupu, briše grupu sa zadanim id brojem
app.delete('/v2/Grupa/:id',function(req,res){  
db.Grupa.findAll({where:{id:req.params.id}}).then(function(r){
		r.forEach(element => {
			element.destroy();
		});
	res.end();        
});
});


//update ruta za Grupu, pošalje joj se id kao parametar i nove vrijednosti u body
//i ažurira onda vrijednost u tabeli za taj id
app.put('/v2/Grupa/:id',function(req,res){  
db.Grupa.update(
				{naziv: req.body.naziv},
				{where: {id:req.params.id}}
				);
				res.end();          
});


//TIP

//post ruta za TIP 
app.post('/v2/Tip',function(req,res){
db.Tip.create({naziv: req.body.naziv}).then(function(r){   
	res.writeHead(200, {'Content-Type': 'application/json'});  
	res.end();
})
});


//get ruta za Tip -> vraća tip 
app.get('/v2/Tip',function(req,res){
db.Tip.findAll().then(function(r){
		//var tipovi = [];
		//for(var i = 0; i < r.length; i++)    tipovi.push(r[i].naziv);  
		res.write(JSON.stringify(r))
	res.end();        
}); 
});

//delete ruta za Tip, briše tip sa zadanim id brojem
app.delete('/v2/Tip/:id',function(req,res){  
db.Tip.findAll({where:{id:req.params.id}}).then(function(r){
		r.forEach(element => {
			element.destroy();
		});
	res.end();        
});
});


//update ruta za Tip, pošalje joj se id kao parametar i nove vrijednosti u body
//i ažurira onda vrijednost u tabeli za taj id
app.put('/v2/Tip/:id',function(req,res){  
db.Tip.update(
				{naziv: req.body.naziv},
				{where: {id:req.params.id}}
				);
				res.end();          
});



///grupa-student 
app.post('/v2/studentGrupa',function(req,res){
var povratna = "";
//dobijem niz studenata i grupu
 var studenti = JSON.stringify(req.body.studenti).replace("[","").replace("]","").replace(/\\/g,"");
 var listaStudenata = studenti.split("},{");

 var grupa = req.body.grupa;
var zaPromise = [];
 for(var i in listaStudenata)
 {  console.log("{" + listaStudenata[i].replace(/"{/,"").replace(/}"/,"") + "}");
	 var jedanRed = JSON.parse("{" + listaStudenata[i].replace(/"{/,"").replace(/}"/,"") + "}");
	//console.log(jedanRed);
	zaPromise.push(db.Student.findAll({where:{index:jedanRed.index}}).then(function(r){   //pretražujem studente sa ovim indexom
		var result;
		if(r.length > 0)   //r je povratna pretrage, ako ima nekih rezultata znači da je našao već u bazi studenta s tim indexom, i onda nećemo da ga dodajemo već vraća ovu poruku
		{
			povratna += "Student "+ jedanRed.ime +" nije kreiran jer postoji student "+ r[0].ime +" sa istim indexom " + jedanRed.index + "\n";
		   // res.writeHead(200, {'Content-Type': 'application/json'});  
			//res.write(JSON.stringify(result));
			//res.end();  
				 
			zaPromise.push( db.Grupa.findAll({where:{naziv:grupa}}).then(function(g){ 
					grupaID = g[0].id;  
					zaPromise.push(db.GrupaStudent.findAll({where:{studentID:r[0].id}}).then(function(k){
						if(k.length > 0)
						{
							zaPromise.push(db.GrupaStudent.update(
							
								{studentID: r[0].id, grupaID:grupaID},
								{where:{id:k[0].id}}
							));
						}
						else  zaPromise.push(db.GrupaStudent.create({studentID: r[0].id, grupaID:grupaID}).then(function(r){ 
							//console.log(req.body.studentID);  
							//res.end();
						}));
					}));
				   
		
				}));
		   //res.end();
		}
		else zaPromise.push(db.Student.create({ime: jedanRed.ime, index:jedanRed.index}).then(function(r){    //a ako nemamo u bazi već studenta, onda ga dodamo i vratim prazan string
		   zaPromise.push(db.Student.findAll({where:{index:jedanRed.index}}).then(function(r){ 
				studentID = r[0].id;       
				zaPromise.push(db.Grupa.findAll({where:{naziv:grupa}}).then(function(r){ 
					grupaID = r[0].id;  
					
					zaPromise.push(db.GrupaStudent.create({studentID: studentID, grupaID:grupaID}).then(function(r){ 
					   
					}))
		
				}));
		   
		   
		   
		   
		   
			}));
			//res.end(); 
		}) )
		
	 })
	)
 }
 Promise.all(zaPromise).then(function(k)
 {
	res.writeHead(200, {'Content-Type': 'application/json'});  
	res.write(JSON.stringify(povratna));
	res.end();
	console.log("povratna:" + povratna);
 });

});
module.exports = app.listen(3000);